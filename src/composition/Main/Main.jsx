import './Main.scss'
import ButtonBuy from "../../components/Button/ButtonBuy";
import Cardses from "../../components/Cardses/Cardses";
import {useEffect, useState} from "react";
import {sendRequest} from "../../helpers/sendRequest.js";
import "swiper/swiper-bundle.css";
import ModalCard from "../../components/Cardses/ModalCard/ModalCard";
import PropTypes from "prop-types";

const Main = ({handleFavorite,handleFavoriteLove,favoriteLoves}) => {
    const [cardPost,setCardPost] = useState({})
    const handleCardPost = (post)=> setCardPost(post)
    const [isOpen,setIsOpen] = useState(false)
    const handleModal = ()=>setIsOpen(!isOpen)
    const [cardpoker, setCardpoker] = useState([])
    useEffect(() => {
        sendRequest('/data.json')
            .then((result) => {
                setCardpoker(result)
                console.log(result)
                })
            }, [])
    console.log(cardPost)
        return (

            <main>
                <div className="wrapper-main">
                    <div className="container">
                        <article className="element-left">
                    <span className="element-left_text">
                        Power up your game
                    </span>
                            <h2 className="element-left_title">
                                CYBER KID <br/>INFINITE
                            </h2>
                            <span className="element-left_text">
                        Now Available on PC & Console
                    </span>
                            <div>
                                <ButtonBuy wow btnLeft>BUY NOW</ButtonBuy>
                            </div>
                        </article>
                        <article className="img-right"></article>


                    </div>
                </div>
                <section className="product-center">
                    <article className="product-center_text">
                        <h2>BEST SELLERS</h2>
                        <div>
                            <ButtonBuy wow btnLeft>View All</ButtonBuy>
                        </div>
                    </article>
                    <div>
                        <Cardses card={cardpoker} favoriteLoves={favoriteLoves} handleModal={handleModal} handleCardPost={handleCardPost}  handleFavoriteLove={handleFavoriteLove} />
                    </div>
                </section>
                <ModalCard
                    name={cardPost.name}
                    price={cardPost.price}
                    img={`${cardPost.img}`}
                    article={cardPost.article}
                    color={cardPost.color}
                    isOpen={isOpen}
                    handleClose={handleModal}
                    handleOk={()=>{
                        handleFavorite(cardPost)
                        handleModal()
                    }}
                />
            </main>

        )
}
Main.propTypes={
    handleFavorite:PropTypes.func,
    handleFavoriteLove:PropTypes.func
}
export default Main

