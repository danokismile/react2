import cx from 'classnames'
// import "./Button.scss"
import PropTypes from "prop-types";
import {Link} from "react-router-dom";

const  ButtonBuy =(props)=>{
    const {type, classNames='', children, onClick,underV, wow,btnLeft,to,href, ...restProps} = props
    let Component = href ? 'a' : 'button'
    if (to){
        Component = Link;
    }
    return(
        <Component onClick={onClick}
        className={cx( classNames, {wow:wow}, {"btn-left":btnLeft},{"_box-underline":underV})}
        type={(!href && !to) && type}
        to ={to}
        href={href}
        {...restProps}>
        {children}
        </Component>
    )
}
ButtonBuy.defaultProps ={
    type: "button",
    onClick: () =>{}
}

ButtonBuy.propTypes ={
    type:PropTypes.string,
    href:PropTypes.string,
    to:PropTypes.string,
    classNames:PropTypes.string,
    children:PropTypes.any,
    onClick:PropTypes.func,
    wow:PropTypes.bool,
    btnLeft:PropTypes.bool,
    underV:PropTypes.bool

}
export default ButtonBuy