import cx from 'classnames'
import "./Button.scss"
import PropTypes from "prop-types";
import {Link} from "react-router-dom";


const  Button =(props)=>{
    const {type, classNames='', children, onClick, boxV,underV,to,href, ...restProps} = props
    let ComponentOne = href ? 'a' : 'button'
    if (to){
        ComponentOne = Link;
    }
    return(
        <ComponentOne onClick={onClick}
                   className={cx("button", classNames, {_box:boxV}, {"_box-underline":underV})}
                      type={(!href && !to) && type}
                      to ={to}
                      href={href}
                   {...restProps}>
                     {children}
        </ComponentOne>
    )
}
Button.defaultProps ={
    type: "button",
    onClick: () =>{}
}

Button.propTypes ={
    type:PropTypes.string,
    to:PropTypes.string,
    href:PropTypes.string,
    classNames:PropTypes.string,
    children:PropTypes.any,
    onClick:PropTypes.func,
    boxV:PropTypes.bool,
    underV:PropTypes.bool

}
export default Button