import Modal from "../Modal/Modal.jsx";
import ModalHeader from "../Modal/ModalHeader.jsx";
import ModalBody from "../Modal/ModalBody.jsx";
import ModalWrapper from "../Modal/ModalWrapper.jsx";
import ModalClose from "../Modal/ModalClose.jsx";
import ModalFooter from "../Modal/ModalFooter.jsx";
import PropTypes from "prop-types";
import './ModalImage.scss'
const ModalImage=({title, desc, handleOk, handleClose, isOpen})=>{
    const handleOutSide = (event)=>{
        if (!event.target.closest(".modal")){
            handleClose()
        }
    }
    return(
        <ModalWrapper isOpen={isOpen} handleOutClose={handleOutSide}>
            <Modal>
                <ModalHeader>
                    <ModalClose click={handleClose}/>
                </ModalHeader>
                <ModalBody>
                    <h4>{title}</h4>
                    <p>{desc}</p>
                </ModalBody>
                <ModalFooter textFirst={"ADD TO FAVORITE"}  btnOne={handleOk}/>
            </Modal>
        </ModalWrapper>
    )
}
ModalImage.propTypes ={
    title:PropTypes.string,
    desc:PropTypes.string,
    handleOk:PropTypes.func,
    handleClose:PropTypes.func,
    handleOutSide:PropTypes.func,
    isOpen:PropTypes.bool

}
export default ModalImage