import MovieTV from "./components/MovieTV/MovieTV"
import MovieCinema from "./components/MovieCinema/MovieCinema"
import LeftArrow from "./icons/leftArrow.svg?react"
import {Link} from "react-router-dom";
import {sendRequest} from "../../helpers/sendRequest";
import {useEffect, useState} from "react";
import {API_KEY_3, API_URL} from "../../configs/api";
import './Movies.scss'

const Movies = () => {
	const [films, setFilms] = useState([])
	const [tv, setTv] = useState([])
	useEffect(() => {
		sendRequest(`${API_URL}/discover/movie?api_key=${API_KEY_3}`)
			.then(({results}) => {
				console.log(results);
				setFilms(results)
			})

		sendRequest(`./data.json`)
			.then(({results}) => {
				setTv(results)
			})
	}, [])

	return (
		<div className="container">
			<div className="films__container">
				<div className="films__title">Популярное По TV</div>
				<div className="films__slider">
					<MovieTV date={tv}/>
					<p className="films__link"><Link className="film-link">All TV</Link><LeftArrow/></p>
				</div>
				<div className="films__title">Популярное В кинотеатрах</div>
				<div className="films__slider">
					<MovieCinema date={films}/>
					<p className="films__link"><Link className="film-link">All Cinemas <LeftArrow/></Link></p>
				</div>
			</div>
		</div>
	)
}


export default Movies