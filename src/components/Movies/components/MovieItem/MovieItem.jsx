// import { IMG_URL } from '../../../../configs/api';

const MovieItem = ({item})=>{
    const {img, title} = item
    return(
        <div className="film-poster">
            <img src={img} alt={title}/>
        </div>
    )
}

export default MovieItem