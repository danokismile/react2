import {Swiper,SwiperSlide} from 'swiper/react';
import "swiper/css"
import MovieItem from '../MovieItem/MovieItem';
const MovieTV = ({date})=>{
    const movieItem = date.map((item,index)=>(<SwiperSlide className="film__item"><MovieItem item={item} key={index} /></SwiperSlide>))
    return(
        <Swiper
            slidesPerView={5}
            spaceBetween={16}
            className="films__wrapper"
            navigation={true}
            grabCursor={false}
            draggable={false}
            preventClicksPropagation={true}
            preventClicks={true}
            scrollbar={{draggable: false, hide: true}}
            slideToClickedSlide={false}
            pagination={{clickable: true}}
        >
         {movieItem}
        </Swiper>
    )
}

export default MovieTV