import PropTypes from "prop-types";
import cx from "classnames"
const Modal = ({children, classNames}) =>{
    return(
        <div className={cx("modal", classNames)}>
            <div className={cx("modal-box", classNames)}>
            {children}
            </div>
        </div>
    )
}
Modal.propTypes ={
    children:PropTypes.any,
    classNames:PropTypes.string
}
export default Modal