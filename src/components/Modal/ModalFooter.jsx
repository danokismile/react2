// import ComponentOne from "../Button/Button.jsx";
import PropTypes from "prop-types";
import Button from "../Button/Button";
const ModalFooter =({btnOne, btnTwo, textFirst, textSecond}) =>{
    return(
        <div className="button-wrapper">
            {textFirst && <Button boxV onClick={btnOne}>{textFirst} </Button>}
            {textSecond && <Button underV onClick={btnTwo} >{textSecond} </Button>}
        </div>
    )
}
ModalFooter.propTypes ={
    btnOne:PropTypes.func,
    btnTwo:PropTypes.func,
    textFirst:PropTypes.string,
    textSecond:PropTypes.string
}
export  default  ModalFooter