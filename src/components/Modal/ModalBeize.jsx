import Modal from "./Modal.jsx";
import ModalHeader from "./ModalHeader";
import ModalBody from "./ModalBody";
import ModalWrapper from "./ModalWrapper";
import ModalClose from "./ModalClose";
import ModalFooter from "./ModalFooter";
import PropTypes from "prop-types";

const ModalBeize=({title, desc, handleOk, handleClose, handleCansle, isOpen})=>{
    const handleOutSide = (event)=>{
        if (!event.target.closest(".modal")){
            handleClose()
        }
    }
    return(
        <ModalWrapper isOpen={isOpen} handleOutClose={handleOutSide}>
            <Modal>
                <ModalHeader>
                    <ModalClose click={handleClose}/>
                    <div className="photo">
                        <img src="../../../src/img/0003_ihrovi-komp_yutery.jpg" alt="" className="photo_width"/>
                    </div>
                </ModalHeader>
                <ModalBody>
                    <h4>{title}</h4>
                    <p>{desc}</p>
                </ModalBody>
                <ModalFooter textFirst={"NO, CANCEL"} textSecond={"YES, DELETE"} btnOne={handleOk} btnTwo={handleCansle}/>
            </Modal>
        </ModalWrapper>
    )
}
ModalBeize.propTypes ={
    title:PropTypes.string,
    desc:PropTypes.string,
    handleOk:PropTypes.func,
    handleClose:PropTypes.func,
    handleCansle:PropTypes.func,
    isOpen:PropTypes.bool

}
export default ModalBeize