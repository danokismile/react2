import "./Modal.scss"
import PropTypes from "prop-types";
const ModalWrapper = ({children, isOpen, handleOutClose}) =>{
    return(
        <>
        {isOpen && ( <div className="modal-wrapper" onClick={(event)=>handleOutClose(event)}>{children}</div>)}
        </>
            )
}
ModalWrapper.defaulProps ={
    isOpen:false
}
ModalWrapper.propTypes={
    children:PropTypes.any,
    isOpen:PropTypes.bool,
    handleOutClose:PropTypes.func
}
export default ModalWrapper