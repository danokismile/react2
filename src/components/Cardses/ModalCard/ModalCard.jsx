import Modal from "../../Modal/Modal"
import ModalHeader from "../..//Modal/ModalHeader";
import ModalBody from "../..//Modal/ModalBody";
import ModalWrapper from "../..//Modal/ModalWrapper";
import ModalClose from "../..//Modal/ModalClose";
import ModalFooter from "../..//Modal/ModalFooter";
import PropTypes from "prop-types";

const ModalCard=({handleOk, name,price,img,article,color, handleClose, isOpen})=>{
    const handleOutSide = (event)=>{
        if (!event.target.closest(".modal")){
            handleClose()
        }
    }
    return(
        <ModalWrapper isOpen={isOpen} handleOutClose={handleOutSide}>
            <Modal>
                <ModalHeader>
                    <ModalClose click={handleClose}/>
                    <img src={img} alt={name}/>
                    {color}
                </ModalHeader>
                <ModalBody>
                    <h4>{name}</h4>
                    <p>{price}</p>
                    <p>{article}</p>
                </ModalBody>
                <ModalFooter textFirst="Add to cart" textSecond="Cansel" btnOne={handleOk} btnTwo={handleClose}/>
            </Modal>
        </ModalWrapper>
    )
}
ModalCard.propTypes ={
    handleOk:PropTypes.func,
    handleClose:PropTypes.func,
    isOpen:PropTypes.bool,
    img:PropTypes.string,
    name:PropTypes.string,
    price:PropTypes.string,
    color:PropTypes.string,
    article:PropTypes.number
}
export default ModalCard