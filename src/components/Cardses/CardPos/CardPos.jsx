import {SwiperSlide} from "swiper/react";
import PropTypes from "prop-types";
import '../../Button/Button.scss'
import ButtonBuy from "../../Button/ButtonBuy";
import Detalis from "./icons/free-icon-vision-4233537.svg?react"
import Cart from "./icons/star-regular.svg?react"
import Wish from "./icons/cart-shopping-solid.svg?react"
import Compare from "./icons/free-icon-compare-3526556.svg?react"
import cn from "classnames"

const CardPos = ({item={}, handleModal,handleCardPost,handleFavoriteLove,isActivFavorite}) =>{
    const {id,name,price,img,article,color,gif} = item
    return(
<SwiperSlide>
    <div className="product-wrap">
            <div className="product-image">
                <span>
                    <img src={img} alt="Ваше изображение" className="all-img-card"/>
                    <img src={gif} alt="sl" className="img-sm-24"/>
                        <div className="shadow"></div>
                </span>
                <ButtonBuy className="detail-link"  title="Быстрый просмотр"><Detalis className="pop sup"/></ButtonBuy>
                <div className="actions">
                    <div className="actions-btn" >
                        <ButtonBuy
                            onClick={() => handleFavoriteLove(item)}
                            className={cn("add-to-cart",{selected: isActivFavorite})}
                            title="Добавить в корзину"
                        >
                            <Cart
                                className={cn("os pop",{selected: isActivFavorite})}
                                fill={isActivFavorite ? 'red' : 'green'}
                            />
                        </ButtonBuy>
                        <ButtonBuy onClick={()=>{
                            handleModal()

                            handleCardPost(item)
                        }} className=" add-to-wishlist"  title="Добавить в мои желания"><Wish className="pop"/></ButtonBuy>
                        <ButtonBuy className=" add-to-compare"  title="Добавить для сравнения"><Compare className="pop"/></ButtonBuy>

                    </div>
                </div>
            </div>
        </div>
    <div className="product-list">
        <h3>{name} - ({color})</h3>
        <div className="price">&#8381; {price}</div>
        <div className="art"><p>артикул:</p>{article}</div>
    </div>
</SwiperSlide>
)
}
CardPos.propTypes = {
    item:PropTypes.object,
    handleModal:PropTypes.func,
    handleCardPost:PropTypes.func,
    handleFavoriteLove:PropTypes.func

}
export default CardPos