
import 'swiper/css';
import 'swiper/css/effect-coverflow';
import 'swiper/css/pagination';
import 'swiper/css/navigation'
import './Cardses.scss';

import PropTypes from "prop-types";


import CardPos from "./CardPos/CardPos";
import {EffectCoverflow, Keyboard, Navigation, Pagination} from "swiper/modules";
import {Swiper, SwiperSlide} from "swiper/react";

const Cardses = ({card=[], handleModal, favoriteLoves, handleCardPost,handleFavoriteLove}) => {
    const movieItem = card.map((item,index)=>(
        <SwiperSlide key={index} >
            <CardPos item={item}
                     handleModal={handleModal}
                     handleCardPost={handleCardPost}
                     handleFavoriteLove={handleFavoriteLove}
                     isActivFavorite={favoriteLoves.some((favorite) => favorite.id === item.id)}
            />
        </SwiperSlide>))
    return (
        <>
            <div>
                <Swiper
                    effect={'coverflow'}
                    grabCursor={true}
                    centeredSlides={true}
                    slidesPerView={'auto'}
                    coverflowEffect={{
                        rotate: 50,
                        stretch: 0,
                        depth: 100,
                        modifier: 1,
                        slideShadows: true,
                    }}
                    pagination={{
                        clickable: true,
                    }}
                    modules={[EffectCoverflow, Pagination, Navigation, Keyboard]}
                    navigation={true}
                    className="mySwiper">
                    {movieItem}
                </Swiper>
            </div>
        </>

    );
}
Cardses.propTypes ={
card:PropTypes.array,
handleModal:PropTypes.func,
    handleCardPost:PropTypes.func,
    handleFavoriteLove:PropTypes.func
}
export default Cardses
