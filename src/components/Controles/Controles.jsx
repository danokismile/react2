import PropTypes from "prop-types";
import Button from "../Button/Button";
const Controles = ({openFirstModal, openSecondModal}) =>{
    return(

        <div className="widget-controllers">
            <div className="button-container">
                <Button onClick={openFirstModal}>Open First Modal</Button>
                <Button onClick={ openSecondModal}>Open Second Modal</Button>
            </div>
        </div>

    )
}
Controles.propTypes ={
    openFirstModal:PropTypes.func,
    openSecondModal:PropTypes.func
}
export default Controles