import Header from "./composition/Header/Header";
import Fotter from "./composition/Fotter/Fotter";
import Main from "./composition/Main/Main";
import {useEffect, useState} from "react";

function App() {
    const [favorites, setFavorite] = useState([])
    const handleFavorite = (item) => {setFavorite([...favorites, item])}

    const [favoriteLoves, setFavoriteLove] = useState([])
    useEffect(() => {
        const storedFavorites = localStorage.getItem('favorites');
        const storedFavoriteLoves = localStorage.getItem('favoriteLoves');

        if (storedFavorites) {
            setFavorite(JSON.parse(storedFavorites));
        }

        if (storedFavoriteLoves) {
            setFavoriteLove(JSON.parse(storedFavoriteLoves));
        }
    }, []);

    // Save data to localStorage whenever favorites or favoriteLoves change
    useEffect(() => {
        localStorage.setItem('favorites', JSON.stringify(favorites));
    }, [favorites]);

    useEffect(() => {
        localStorage.setItem('favoriteLoves', JSON.stringify(favoriteLoves));
    }, [favoriteLoves]);

    const handleFavoriteLove =(item)=>{
        const isAddDel = favoriteLoves.some((favoriteLove) => favoriteLove.id === item.id)
        if (isAddDel) {
            setFavoriteLove((favoriteLoves) => favoriteLoves.filter( (favoriteLove) => favoriteLove.id !== item.id ))
        } else {
            setFavoriteLove([...favoriteLoves, item])

        }

    }




    return (
        <>

            {/*<div className="widget-container">*/}
            {/*<Controles openFirstModal={modalClose}*/}
            {/*   openSecondModal={modalCloseImage}/>*/}
            {/*<ModalBeize*/}
            {/*    isOpen={isModal}*/}
            {/*    title={"Product Delete!"}*/}
            {/*    desc={"By clicking the “Yes, Delete” button, PRODUCT NAME will be deleted."}*/}
            {/*    handleOk={modalAdd}*/}
            {/*    handleClose={modalClose}*/}
            {/*    handleCansle={modalClose}*/}
            {/*></ModalBeize>*/}

            {/*    <ModalImage*/}
            {/*        isOpen={isModalImage}*/}
            {/*        title={'Add Product “NAME”'}*/}
            {/*        desc={"Description for you product"}*/}
            {/*        handleClose={modalCloseImage}*/}
            {/*        handleOk={modalCloseImage}*/}
            {/*    ></ModalImage>*/}
            {/*</div>*/}

            <Header favorite={favorites} favoriteLove={favoriteLoves}/>
            <Main favoriteLoves={favoriteLoves} handleFavorite={handleFavorite} handleFavoriteLove={handleFavoriteLove}/>
            <Fotter/>
        </>
    )
}
export default App
